/**
Copyright (c) 2016 Daniel Wanner

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#ifndef DUKBIND_DUKFUNCTION_HPP
#define DUKBIND_DUKFUNCTION_HPP

#include<functional>
#include<string>
#include<iostream>
#include<duktape/duktape.h>
#include<dukbind/detail.hpp>

namespace dukbind
{
    class DukFunction
    {
    public:
        DukFunction();
        ~DukFunction();

        template<unsigned int I_Unique = 0, typename T_Return, typename ... T_Params>
        void bindFunction(T_Return(*function_item)(T_Params ...), std::string name_item)
        {
            std::function<T_Return(T_Params ...)> function_object(function_item);
            detail::duk_function_data = &function_object;
            detail::duk_function_proxy<I_Unique, T_Return, T_Params ...>(NULL);
            duk_function_t ret(detail::duk_function_proxy<I_Unique, T_Return, T_Params ...>);
            this->m_Function = ret;
            this->m_Name = name_item;
        }
        template<unsigned int I_Unique = 0, typename T_Return, typename ... T_Params>
        void bindFunction(std::function<T_Return(T_Params ...)> function_item, std::string name_item)
        {
            detail::duk_function_data = &function_item;
            detail::duk_function_proxy<I_Unique, T_Return, T_Params ...>(NULL);
            duk_function_t ret(detail::duk_function_proxy<I_Unique, T_Return, T_Params ...>);
            this->m_Function = ret;
            this->m_Name = name_item;
        }

        std::function<duk_ret_t(duk_context*)> getFunction();
        std::string getName();
    private:
        std::function<duk_ret_t(duk_context*)> m_Function;
        std::string m_Name;
    };
}

#endif // DUKFUNCTION_HPP
