/**
Copyright (c) 2016 Daniel Wanner

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#ifndef DUKBIND_DUKCONTEXT_HPP
#define DUKBIND_DUKCONTEXT_HPP

#include<string>

#include<duktape/duktape.h>
#include<dukbind/DukFunction.hpp>
#include<dukbind/DukObject.hpp>
#include<dukbind/DukModule.hpp>

namespace dukbind
{
    class DukContext
    {
    public:
        DukContext();
        ~DukContext();

        void eval(std::string code_item);
        void evalFile(std::string filename_item);

        void add(DukFunction& function_item);
        void add(DukModule& module_item);

        duk_context* getHandle();
    private:
        duk_context* m_Handle;
    };
}

#endif // DUKCONTEXT_HPP
